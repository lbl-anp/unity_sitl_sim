using UnityEngine;

public class CoordinateConverter : MonoBehaviour
{
    [SerializeField] private double latitude;
    [SerializeField] private double longitude;

    public double SemiMajorAxis => 6378137; // In meters, WGS84 ellipsoid
    public double Flattening => 1.0 / 298.257223563; // WGS84 ellipsoid

    public static CoordinateConverter Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public (double, double, double) ToLatLonAlt(Transform unityTransform)
    {
        double e2 = 2 * Flattening - System.Math.Pow(Flattening, 2);

        double N = SemiMajorAxis / System.Math.Sqrt(1 - e2 * System.Math.Pow(System.Math.Sin(System.Math.PI / 180.0 * latitude), 2));
        double M = SemiMajorAxis * (1 - e2) / System.Math.Pow(1 - e2 * System.Math.Pow(System.Math.Sin(System.Math.PI / 180.0 * latitude), 2), 1.5);

        double deltaLatitude = (unityTransform.position.z / M) * (180.0 / System.Math.PI);
        double deltaLongitude = (unityTransform.position.x / (N * System.Math.Cos(System.Math.PI / 180.0 * latitude))) * (180.0 / System.Math.PI);

        double newLatitude = latitude + deltaLatitude;
        double newLongitude = longitude + deltaLongitude;
        double newAltitude = unityTransform.position.y;

        return (newLatitude, newLongitude, newAltitude);
    }
}
