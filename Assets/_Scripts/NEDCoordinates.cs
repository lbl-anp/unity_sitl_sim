using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;

public class NEDCoordinates : MonoBehaviour
{
    private Rigidbody uavRigidbody;
    // Start is called before the first frame update
    void Start()
    {
        uavRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = transform.position;
        Quaternion rotation = transform.rotation;
        Vector3 rotEu1 = rotation.eulerAngles;
        Vector3<NED> posNED = position.To<NED>();
        print("NED position: " + posNED);
        print(posNED.x);
        Quaternion<NED> rotationNED = rotation.To<NED>();
        Quaternion<NEDLocal> rotationNEDLocal = rotation.To<NEDLocal>();
        Quaternion<FRD> rotationFRD = rotation.To<FRD>();
        // to Quaternion
        Quaternion q = new Quaternion(rotationNED.x, rotationNED.y, rotationNED.z, rotationNED.w);
        Quaternion qLocal = new Quaternion(rotationNEDLocal.x, rotationNEDLocal.y, rotationNEDLocal.z, rotationNEDLocal.w);
        Quaternion qFRD = new Quaternion(rotationFRD.x, rotationFRD.y, rotationFRD.z, rotationFRD.w);
        // print("Quaternion: " + q);
        // print("NED rotation: " + rotationNED);
        Vector3 euler = q.eulerAngles * Mathf.Deg2Rad;
        Vector3 eulerLocal = qLocal.eulerAngles * Mathf.Deg2Rad;
        Vector3 eulerFRD = qFRD.eulerAngles * Mathf.Deg2Rad;
        print("EulerNED: " + euler * Mathf.Rad2Deg);
        print("EulerNEDLocal: " + eulerLocal * Mathf.Rad2Deg);
        print("Euler: " + transform.eulerAngles);
        print("EulerLocal: " + transform.localEulerAngles);
        print("Eu1: " + rotEu1);
        print("EulerFRD: " + eulerFRD * Mathf.Rad2Deg);

        Vector3 currentVelocity = uavRigidbody.velocity;
        Vector3<NED> velocityNED = currentVelocity.To<NED>();
        // print("NED velocity: " + velocityNED);
        // print("Velocity: " + currentVelocity);
    }
}
