using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadcopterController : MonoBehaviour
{
    public GameObject FL; // Front-left motor
    public GameObject FR; // Front-right motor
    public GameObject BL; // Back-left motor
    public GameObject BR; // Back-right motor

    public GameObject FL_meter;
    public GameObject FR_meter;
    public GameObject BL_meter;
    public GameObject BR_meter;
    public bool applyForces = true;


    private Rigidbody quadcopterRigidbody;

    // PWM input values for the motor
    public float pwmFL = 1500f;
    public float pwmFR = 1500f;
    public float pwmBL = 1500f;
    public float pwmBR = 1500f;

    // Constants to convert PWM values to thrust
    private const float PWM_MIN = 1000f;
    private const float PWM_MAX = 2000f;
    private const float THRUST_MIN = 0f;
    public float THRUST_MAX = 10f;
    public float torqueMultiplier = 0.01f;

    void Start()
    {
        quadcopterRigidbody = GetComponent<Rigidbody>();
    }
    public void SetPWMs(float pwmFR, float pwmBL, float pwmFL, float pwmBR)
    {
        this.pwmFL = pwmFL;
        this.pwmFR = pwmFR;
        this.pwmBL = pwmBL;
        this.pwmBR = pwmBR;
    }

    void FixedUpdate()
    {
        // Convert PWM to thrust
        float thrustFL = ConvertPwmToThrust(pwmFL);
        float thrustFR = ConvertPwmToThrust(pwmFR);
        float thrustBL = ConvertPwmToThrust(pwmBL);
        float thrustBR = ConvertPwmToThrust(pwmBR);

        // Apply forces at motor positions
        // print FL motor position and thruse
        // print("FL");
        // print(FL.transform.position);
        // Apply forces at motor positions
        if(applyForces){
            quadcopterRigidbody.AddForceAtPosition(FL.transform.up * thrustFL, transform.TransformPoint(FL.transform.localPosition));
            quadcopterRigidbody.AddForceAtPosition(FR.transform.up * thrustFR, transform.TransformPoint(FR.transform.localPosition));
            quadcopterRigidbody.AddForceAtPosition(BL.transform.up * thrustBL, transform.TransformPoint(BL.transform.localPosition));
            quadcopterRigidbody.AddForceAtPosition(BR.transform.up * thrustBR, transform.TransformPoint(BR.transform.localPosition));
        }

        //scale each motor-meter in y direction
        FL_meter.transform.localScale = new Vector3(FL_meter.transform.localScale.x, thrustFL*12/THRUST_MAX, FL_meter.transform.localScale.z);
        FR_meter.transform.localScale = new Vector3(FR_meter.transform.localScale.x, thrustFR*12/THRUST_MAX, FR_meter.transform.localScale.z);
        BL_meter.transform.localScale = new Vector3(BL_meter.transform.localScale.x, thrustBL*12/THRUST_MAX, BL_meter.transform.localScale.z);
        BR_meter.transform.localScale = new Vector3(BR_meter.transform.localScale.x, thrustBR*12/THRUST_MAX, BR_meter.transform.localScale.z);

        // Calculate net rotation force based on motor spinning directions
        // Assuming motors 1 (FL) and 3 (BR) spin clockwise, and motors 2 (FR) and 4 (BL) spin counterclockwise
        float rotationForce = 0f;
        rotationForce -= Vector3.Cross(transform.TransformDirection(FL.transform.localPosition), Vector3.up).magnitude * thrustFL;  // Clockwise
        rotationForce += Vector3.Cross(transform.TransformDirection(FR.transform.localPosition), Vector3.up).magnitude * thrustFR;  // Counterclockwise
        rotationForce += Vector3.Cross(transform.TransformDirection(BL.transform.localPosition), Vector3.up).magnitude * thrustBL;  // Counterclockwise
        rotationForce -= Vector3.Cross(transform.TransformDirection(BR.transform.localPosition), Vector3.up).magnitude * thrustBR;  // Clockwise
        // print("rotationForce: " + rotationForce);
        // Apply torque to quadcopter rigidbody
        if(applyForces){
            quadcopterRigidbody.AddTorque(transform.up * rotationForce * torqueMultiplier);
        }
    }

    private float ConvertPwmToThrust(float pwmValue)
    {
        return Mathf.Lerp(THRUST_MIN, THRUST_MAX, (pwmValue - PWM_MIN) / (PWM_MAX - PWM_MIN));
    }
}
