using UnityEngine;

public class ObjectWiggler : MonoBehaviour
{
    public float speed = 2f;  // Speed of the wiggle motion
    public float amplitude = 1f;  // Amplitude of the wiggle motion
    public float frequency = 1f;  // Frequency of the wiggle motion

    private Vector3 startPos;  // Starting position of the GameObject
    private Quaternion startRot;  // Starting rotation of the GameObject

    private void Start()
    {
        startPos = transform.position;
        startRot = transform.rotation;
    }

    private void Update()
    {
        // Calculate the horizontal position offset based on time and frequency
        float xOffset = Mathf.Sin(Time.time * speed * frequency) * amplitude;

        // Calculate the rotation offset based on time and frequency
        float rotOffset = Mathf.Sin(Time.time * speed * frequency * 2f * Mathf.PI) * amplitude;

        // Set the new position of the GameObject with the calculated offset
        transform.position = new Vector3(startPos.x + xOffset, startPos.y, startPos.z);

        // Set the new rotation of the GameObject with the calculated rotation offset
        transform.rotation = startRot * Quaternion.Euler(rotOffset*2, rotOffset, rotOffset);
    }
}
