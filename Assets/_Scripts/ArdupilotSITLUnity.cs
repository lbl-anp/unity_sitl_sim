using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;


public class ArdupilotSITLUnity : MonoBehaviour
{
    public GameObject uav;

    private Rigidbody uavRigidbody;
    private UdpClient udpClient;
    private IPEndPoint endPoint;
    private int frameCount = 0;
    private Vector3<NED> previousVelocity = new Vector3<NED>(0, 0, 0);
    private const int SITL_PORT = 9002;
    [SerializeField] private QuadcopterController quadcopterController;

    void Start()
    {
        uavRigidbody = uav.GetComponent<Rigidbody>();

        udpClient = new UdpClient(SITL_PORT);
        endPoint = new IPEndPoint(IPAddress.Any, SITL_PORT);
        udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);
    }

    void FixedUpdate()
    {
        SendDataToSITL();
    }


    private void ReceiveData(IAsyncResult result)
    {
        byte[] data = udpClient.EndReceive(result, ref endPoint);
        ProcessSITLData(data);
        udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);
    }

    private void ProcessSITLData(byte[] data)
    {
        // Check if data has enough length for the header
        if (data.Length >= 8)
        {
            ushort magic = BitConverter.ToUInt16(data, 0);
            ushort frame_rate = BitConverter.ToUInt16(data, 2);
            uint frame_count = BitConverter.ToUInt32(data, 4);

            if (magic == 18458 || magic == 29569)
            {
                int numChannels = (magic == 18458) ? 16 : 32;

                // Check if data has enough length for the PWM values
                if (data.Length >= 8 + 2 * numChannels)
                {
                    ushort[] pwm = new ushort[numChannels];
                    for (int i = 0; i < numChannels; i++)
                    {
                        pwm[i] = BitConverter.ToUInt16(data, 8 + 2 * i);
                    }
                    quadcopterController.SetPWMs(pwm[0], pwm[1], pwm[2], pwm[3]);
                    // Debugging: Print PWM values
                    // print("Received PWM values:");
                    // for (int i = 0; i < pwm.Length; i++)
                    // {
                    //     if (pwm[i] != 0){
                    //     print($"pwm[{i}]: {pwm[i]}");
                    //     }
                    // }
                }
            }
        }
    }

    private void SendDataToSITL()
    {

        double timestamp = Time.time;
        // Notes
        //gyro = [0;0;0]; % (rad/sec)
        //attitude = [0;0;0]; % (radians) (roll, pitch, yaw)
        //accel = [0;0;0]; % (m/s^2) body frame
        //velocity = [0;0;0]; % (m/s) earth frame
        //position = [0;0;0]; % (m) earth frame
        //bf_velo = [0;0;0]; % (m/s) body frame

        Vector3 attitude = uav.transform.eulerAngles * Mathf.Deg2Rad;
        Vector3<NED> posNED = uav.transform.position.To<NED>();
        Quaternion<NED> rotationNED = uav.transform.localRotation.To<NED>();
        Quaternion q = new Quaternion(rotationNED.x, rotationNED.y, rotationNED.z, rotationNED.w);
        Vector3 eulerNED = q.eulerAngles * Mathf.Deg2Rad;
        Vector3<NED> currentVelocity = uavRigidbody.velocity.To<NED>();
        Vector3<NED> accel_body = (currentVelocity - previousVelocity) / Time.fixedDeltaTime;
        previousVelocity = currentVelocity;
        Vector3<NED> gyro = uavRigidbody.angularVelocity.To<NED>();


        double lat, lon, alt;
        (lat, lon, alt) = CoordinateConverter.Instance.ToLatLonAlt(uav.transform);
        string json = $"{{" +
            $"\"timestamp\":{timestamp}," +
            $"\"imu\":{{" +
                $"\"gyro\":[{gyro.x},{gyro.y},{gyro.z}]," +
                $"\"accel_body\":[{accel_body.x},{accel_body.y},{accel_body.z}]" +
            $"}}," +
            $"\"position\":[{posNED.x},{posNED.y},{posNED.z}]," +
            $"\"attitude\":[{-attitude.z},{-attitude.x},{attitude.y}]," +
            $"\"velocity\":[{currentVelocity.x},{currentVelocity.y},{currentVelocity.z}]," +
            $"\"gps\":{{" +
                $"\"hdop\":0.0," +
                $"\"vdop\":0.0," +
                $"\"lat\":{0}," +
                $"\"lng\":{0}," +
                $"\"altitude\":{123}," +
                $"\"fix_type\":3," +
                $"\"satellites_visible\":10" + 
            $"}}" +
        $"}}\n";
                // $"\"course\":{course}," +
                // $"\"speed\":{velocity.magnitude}," +

        byte[] jsonData = Encoding.ASCII.GetBytes(json);
        frameCount++;
        // print("Sending data to SITL");
        // print(json);
        udpClient.Send(jsonData, jsonData.Length, endPoint);
    }

    void OnDestroy()
    {
        if (udpClient != null)
        {
            udpClient.Close();
        }
    }
}
